var words = ["Designer", "Developer", "programmer", "Gamer", "Blogger"]
var counter = 0;
var currentIndex = getRandomInt(0, words.length - 1);
var text = document.querySelector("#type-it");

var stepInterval = setInterval(() => { step(); }, 200);
var delInterval = null;
var delTimeout = null;

function getRandomInt(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) +min; 
}

function delIntervalCallback(){
    delInterval = setInterval(() => {del(); }, 100);
    clearTimeout(delTimeout);
}

function del(){
    if(counter == 0){
        let newIndex = getRandomInt(0, words.length - 1);
        while(newIndex == currentIndex){
            newIndex = getRandomInt(0, words.length - 1)
        }
        currentIndex = newIndex;
        clearInterval(delInterval);
        stepInterval = setInterval(() =>{step();}, 200);
    }   
    else{
        text.textContent = text.textContent.slice(0, -1)
        counter--;
    }
}

function step(){ 
    if(counter>= words[currentIndex].length){
    clearInterval(stepInterval);
    delTimeout = setTimeout(()=> {delIntervalCallback(); }, 2000);
   }

   else{
    text.textContent += words[currentIndex][counter];
    counter++;
   }
}

/// portfolio lightbox

const  portfolioBoxBtn = document.querySelectorAll(".portfolio-box .slider-btn"),
       totalPortfolioBoxBtn = portfolioBoxBtn.length,
       portfolioBox = document.querySelectorAll(".portfolio-box"),
       totalPortfolioBox = portfolioBox.length,
       lightbox = document.querySelector(".lightbox"),
       lightBoxImg = lightbox.querySelector(".lightbox-img"),
       lightBoxClose =lightbox.querySelector(".lightbox-close");

let itemIndex = 0;

for(let i=0; i<totalPortfolioBox; i++){
    (function (index){
        portfolioBoxBtn[index].addEventListener("click", function(){
            itemIndex = index;
            togglelightbox();
            changeItem();
        });
        portfolioBox[index].addEventListener("click", function(){
            itemIndex = index;
        });
    })(i);
} 

function togglelightbox(){
    lightbox.classList.toggle("open");
}

function changeItem(){
    const imgSrc = portfolioBox[itemIndex].querySelector(".portfolio-img img").getAttribute("src");
    lightBoxImg.src = imgSrc;
}

function prevItem() {
    itemIndex = (itemIndex -1 +totalPortfolioBox) % totalPortfolioBox;
    changeItem();
}

function nextItem() {
    itemIndex = (itemIndex + 1) % totalPortfolioBox;
    changeItem();
}

lightBoxClose.addEventListener("click", function(){
    closeLightBox();
});

function closeLightBox(){
    togglelightbox();
}

lightBoxImg.addEventListener("click", function(){
    itemIndex = (itemIndex + 1) % totalPortfolioBox;
    changeItem();
});

lightbox.addEventListener("click", function(event){
    if(event.target == lightbox){
        closeLightBox();
    }
});

document.addEventListener("keydown", function(event){
    if(event.key == "Escape"){
        closeLightBox();
    }
});

const nameEl = document.querySelector("#name");
const emailEl = document.querySelector("#email");
const companyNameEl = document.querySelector("#company-name");
const messageEl = document.querySelector("#message");

const form = document.querySelector("#submit-form");

function checkValidations() {
  let letters = /^[a-zA-Z\s]*$/;
  const name = nameEl.value.trim();
  const email = emailEl.value.trim();
  const companyName = companyNameEl.value.trim();
  const message = messageEl.value.trim();
  if (name === "") {
     document.querySelector(".name-error").classList.add("error");
      document.querySelector(".name-error").innerText =
        "Please fill out this field!";
  } else {
    if (!letters.test(name)) {
      document.querySelector(".name-error").classList.add("error");
      document.querySelector(".name-error").innerText =
        "Please enter only characters!";
    } else {
      
    }
  }
  if (email === "") {
     document.querySelector(".name-error").classList.add("error");
      document.querySelector(".name-error").innerText =
        "Please fill out this field!";
  } else {
    if (!letters.test(name)) {
      document.querySelector(".name-error").classList.add("error");
      document.querySelector(".name-error").innerText =
        "Please enter only characters!";
    } else {
      
    }
  }
}

function reset() {
  nameEl = "";
  emailEl = "";
  companyNameEl = "";
  messageEl = "";
  document.querySelector(".name-error").innerText = "";
}
